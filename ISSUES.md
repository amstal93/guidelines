# Working with issues

Either in Gitlab or other issue tracking tool, here's a simple guideline on how to work with them:

* The one who reports an issue is most likely the one who SHOULD close it, given that issues MUST be tested/approved for completion.
* Whoever is working with an issue MUST have it assigned (either by them or by someone else).
* If an issue is already assigned means that that person could be working with it, so before self-assigning such issue ask the current assignee if you can take it.
* Issues MUST go through the tags `todo`, `doing` and `done`.

## Tags

* `todo`: set to be done in the current sprint.
* `doing`: already doing it by the assignee.
* `done`: already done by the assignee, to be closed/reassigned by the issuer.
